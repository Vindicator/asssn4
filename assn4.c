#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<crtdbg.h>
#include<windows.h>
#define MAX_TITLE_FILE 31
#define MAX_LENGTH 200
#define MAX_SAVE 10

int menu_alter(char a[MAX_TITLE_FILE], int b);
int alloc_value(FILE* infile, char*** _lntr, char*** _lnan, int* stats);
void free_value(char*** a, int lnum);
void wer_cal_line(char*** _lntr, char*** _lnan, int lnum, int *cstats, int *line);
int word_num(char*** ln, int b);
void word_alloc(char*** a, char*** b, int c, int d);
int matrix_cal(char*** a, char*** b, int***c, int trwdn, int anwdn);

int main(void)
{
	int choice, i;
	int stats = 0;
	int cstats = 0;
	int lnum = 0;
	int lstats;
	char ifn[MAX_TITLE_FILE];
	char imt;
	char **_lntr;
	char **_lnan;
	FILE *infile;
	while (1)
	{
		system("cls");
		choice = menu_alter(ifn, stats);
		if (choice == 0)
		{
			if (stats == 1)
			{
				for (i = 0; i <= lnum; i++)
					free(_lntr[i]);
				free(_lntr);
				for (i = 0; i <= lnum; i++)
					free(_lnan[i]);
				free(_lnan);
			}
			break;
		}
		else if (choice == 1)
		{
			printf("파일 이름을 입력해 주세요\n");
			scanf("%s", ifn);
			infile = fopen(ifn, "r");
			if (infile == NULL)
			{
				printf("파일이 없습니다.\n");
				sleep(5000);
				continue;
			}
			else if (fscanf(infile, "%c", &imt) == EOF)
			{
				printf("파일이 비었습니다.\n");
				sleep(5000);
				continue;
			}
			else
			{
				rewind(infile);
				if (stats == 1)
				{
					for (i = 0; i <= lnum; i++)
						free(_lntr[i]);
					free(_lntr);
					for (i = 0; i <= lnum; i++)
						free(_lnan[i]);
					free(_lnan);
					printf(" 알림: %s에 대한 동적 할당을 해제하였습니다.\n", ifn);
				}
				lnum = alloc_value(infile, &_lntr, &_lnan, &stats);
				fclose(infile);
				printf("%s에서 총 %d줄을 읽었습니다.\n", ifn, lnum);
				printf("파일 읽기를 완료하였습니다.\n");
				sleep(5000);
				continue;
			}
		}
		else if (choice == 2)
		{
			if (stats == 0)
			{
				printf("파일을 먼저 입력해주십시오\n");
				sleep(5000);
				continue;
			}
			else
			{

				continue;
			}
		}
		else if (choice == 3)
		{
			if (stats == 0)
			{
				printf("파일을 먼저 입력해주십시오\n");
				sleep(5000);
				continue;
			}
			else
			{
				wer_cal_line(&_lntr, &_lnan, lnum, &cstats, &lstats);
				sleep(5000);
				continue;
			}
		}
		else if (choice == 4)
		{
			if (stats == 0)
			{
				printf("파일을 먼저 입력해주십시오\n");
				sleep(5000);
				continue;
			}
			else
			{

				continue;
			}
		}
		else
		{
			printf("올바르지 않은 숫자입니다. 다시 시도해 주세요\n");
			_getch();
			continue;
		}
	}
	_CrtDumpMemoryLeaks();
	return 0;
}
int menu_alter(char a[MAX_TITLE_FILE], int b)
{
	int i = 0;
	printf("==================================\n");
	if (!b)
		printf("1. 파일 입력\n");
	else
		printf("1. 파일 변경 - 현재 파일: %s\n", a);
	printf("2. 입력 파일 전체 분석\n");
	printf("3. 단일 문장 분석\n");
	printf("4. 분석 결과 출력\n");
	printf("0. 종료\n");
	printf("==================================\n");
	printf("메뉴 선택 > ");
	scanf("%d", &i);
	return i;
}
int alloc_value(FILE* infile, char*** _lntr, char*** _lnan, int* stats)
{
	char tmps[MAX_LENGTH];
	char *temp1;
	char *temp2;
	int lnum = 0;
	int lnc = 0;
	int i;
	char *delimeter = "	\n";
	while (!feof(infile))
	{
		fgets(tmps, MAX_LENGTH, infile);
		lnum++;
	}
	rewind(infile);
	*_lntr = (char**)calloc(lnum + 1, sizeof(char*));
	*_lnan = (char**)calloc(lnum + 1, sizeof(char*));
	while (!feof(infile))
	{
		fgets(tmps, MAX_LENGTH, infile);
		temp1 = strtok(tmps, delimeter);
		(*_lntr)[lnc] = (char*)calloc(strlen(temp1) + 1, sizeof(char));
		strcpy((*_lntr)[lnc], temp1);
		(*_lntr)[lnc][strlen(temp1)] = '\0';
		temp2 = strtok(NULL, delimeter);
		(*_lnan)[lnc] = (char*)calloc(strlen(temp2) + 1, sizeof(char));
		strcpy((*_lnan)[lnc], temp2);
		(*_lnan)[lnc][strlen(temp2)] = '\0';
		lnc++;
	}
	for (i = 0; i < lnum; i++)//할당, 입력 확인용. 삭제예정
		printf("%s\n", (*_lntr)[i]);
	for (i = 0; i < lnum; i++)//할당,입력 확인용. 삭제예정
		printf("%s\n", (*_lnan)[i]);
	fclose(infile);
	*stats = 1;
	return lnum;
}
void free_value(char*** a, int lnum)
{
	int i;
	for (i = 0; i < lnum; i++)
		free((*a)[i]);
	free((*a));
}
void wer_cal_line(char*** _lntr, char*** _lnan, int lnum, int *cstats, int *line)
{
	int i, j;
	int lcac;
	int trwdn;
	int anwdn;
	int werscore;
	char** trs;
	char** ans;
	int** nomat;
	printf("현재 파일은 %d줄까지 존재합니다.\n", lnum);
	printf("몇 번째 줄에 대해서 분석하시겠습니까?\n");
	scanf("%d", &lcac);
	if (lcac > lnum || lcac <= 0)
	{
		printf("입력 파일에 없는 줄 번호입니다.\n");
	}
	else
	{
		lcac -= 1;
		printf("MT output: %s\n", (*_lntr)[lcac]);
		printf("Reference: %s\n", (*_lnan)[lcac]);
		trwdn = word_num(_lntr, lcac);
		anwdn = word_num(_lnan, lcac);
		word_alloc(_lntr, &trs, trwdn, lcac);
		word_alloc(_lnan, &ans, anwdn, lcac);
		nomat = (int**)calloc(trwdn + 1, sizeof(int*));
		for (i = 0; i <= trwdn; i++)
			nomat[i] = (int*)calloc(anwdn + 1, sizeof(int));
		werscore = matrix_cal(&trs, &ans, &nomat, trwdn, anwdn);
		for (i = 0; i < (trwdn + 1); i++)
		{
			for (j = 0; j <= anwdn; j++)
				printf("%d ", nomat[i][j]);
			printf("\n");
		}
		printf("%d: Minimum edit distance=%d, WER score=%.3f\n", lcac + 1, nomat[trwdn][anwdn], (float)werscore / (float)anwdn);



		printf("\n단어분리 검증\n");
		for (i = 0; i < trwdn; i++)
			printf("%s/", trs[i]);
		printf("\n");
		for (i = 0; i < anwdn; i++)
			printf("%s/", ans[i]);
		printf("\n");
		printf("무결성 1: %s\n", (*_lntr)[lcac]);
		printf("무결성 2: %s\n", (*_lnan)[lcac]);
		printf("TR WN: %d, AN WN: %d\n", trwdn, anwdn); //디버깅용 출력문, 삭제 예정

		for (i = 0; i <= trwdn; i++)
			free(nomat[i]);
		free(nomat);
		/*for (i = 0; i <= trwdn; i++)
			free(trs[i]);
		free(trs);
		for (i = 0; i <= anwdn; i++)
			free(ans[i]);
		free(ans);*/
		*cstats = 1;
	}
}
int word_num(char*** ln, int b)
{
	char *temp;
	char *token;
	int retval = 0;
	temp = (char*)calloc(strlen((*ln)[b]), sizeof(char));
	strcpy(temp, (*ln)[b]);
	token = strtok(temp, " ");
	while (token != NULL)
	{
		retval++;
		token = strtok(NULL, " ");
	}
	return retval;
	free(temp);
}
void word_alloc(char*** a, char*** b, int c, int d)
{
	int i = 0;
	char *tmp;
	char *stp;
	stp = (char*)calloc(strlen((*a)[d]), sizeof(char));
	strcpy(stp, (*a)[d]);
	(*b) = (char**)calloc(c, sizeof(char*));
	tmp = strtok(stp, " ");
	while (tmp != NULL)
	{
		(*b)[i] = (char*)calloc(strlen(tmp), sizeof(char));
		strcpy((*b)[i], tmp);
		tmp = strtok(NULL, " ");
		i++;
	}
	//free(stp);
}
int matrix_cal(char*** a, char*** b, int***c, int trwdn, int anwdn)
{
	int i, j, k, retval;
	int com[3] = { 0, };
	int add1, gest = 0;
	for (i = 0; i <= trwdn; i++)

		(*c)[i][0] = i;
	for (j = 0; j <= anwdn; j++)
		(*c)[0][j] = j;

	for (i = 1; i <= trwdn; i++)
	{
		for (j = 1; j <= anwdn; j++)
		{
			if (!strcmp((*a)[i - 1], (*b)[j - 1]))
				add1 = 0;
			else
				add1 = 1;
			com[0] = (*c)[i - 1][j - 1] + add1;
			com[1] = (*c)[i - 1][j] + 1;
			com[2] = (*c)[i][j - 1] + 1;
			gest = com[0];
			for (k = 0; k < 3; k++)
				if (com[k]<gest)
					gest = com[k];
			(*c)[i][j] = gest;
		}
	}
	retval = (*c)[trwdn][anwdn];
	return retval;
}